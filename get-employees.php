<!--
Code gets all the employees plus their IDs.
Adapted from the code by @cleverappz at https://gist.github.com/cleverappz/387466d74573973cf9b9
-->

<?php
date_default_timezone_set("Australia/Sydney");
$actual_db = array();
$total_count = 0;

$endpoint = "ENTER ENDPOINT HERE";
$url = "ENTER URL HERE";
$token = "ENTER TOKEN HERE";

while(true){
    $fetched = dp_api(
        $endpoint,
        $url ,
        $token ,
        array(
            'max' => 500 ,
            'start' => $total_count
        )
    );
    $last_count = count($fetched);
    $total_count += count($fetched);
    $actual_db = array_merge($actual_db , $fetched);
    if($last_count < 500)
        break;
}
$employees = array();


foreach($actual_db as $arr){
    $employee = array();
    $employee['name'] = $arr['DisplayName'];
    $employee['deputyID'] = $arr['Id'];
    array_push($employees, $employee);
}

print_r($employees);




/**
*API Get Functions
**/

function dp_api($endpoint  , $url , $token , $postvars = null){
    $get = dp_wget("https://" . $endpoint . "/api/v1/" . $url
    , $postvars?json_encode($postvars):null
    , array(
        CURLOPT_HTTPHEADER => array(
              'Content-type: application/json'
            , 'Accept: application/json'
            , 'Authorization : OAuth ' . $token
            , 'dp-meta-option : none'
            )
        )
    );

    return json_decode($get , true);
}


function dp_wget($url , $postvars = null , $curlOpts = array()){
        // Purpose : refresh session token every hour
        $piTrCurlHandle = curl_init();
        curl_setopt($piTrCurlHandle, CURLOPT_HTTPGET, 1);
        curl_setopt($piTrCurlHandle, CURLOPT_RESUME_FROM, 0);
        curl_setopt($piTrCurlHandle, CURLOPT_URL, $url);
        curl_setopt($piTrCurlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($piTrCurlHandle, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($piTrCurlHandle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($piTrCurlHandle, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($piTrCurlHandle, CURLOPT_TIMEOUT, 500); // 500 secs

        if($postvars){
            curl_setopt($piTrCurlHandle, CURLOPT_POST, 1);
            curl_setopt($piTrCurlHandle, CURLOPT_POSTFIELDS, $postvars);
        }

        if($curlOpts)
        foreach($curlOpts as $opt=>$value)
            curl_setopt($piTrCurlHandle, $opt, $value);


        $data = curl_exec($piTrCurlHandle);
        return $data;
}
